package com.shchyrov.app.skysofttest;

import android.content.Intent;
import android.os.Bundle;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.shchyrov.app.skysofttest.mvp.auth.FragmentAuth;
import com.shchyrov.app.skysofttest.util.FragmentUtil;
import com.shchyrov.app.skysofttest.util.PreferenceUtil;

import androidx.appcompat.app.AppCompatActivity;

public class AuthActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        if (PreferenceUtil.getUser(this) != null) {
            startActivity(new Intent(AuthActivity.this, MainActivity.class));
            finish();
            return;
        }
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        FragmentUtil.openFragment(this, R.id.container_auth, new FragmentAuth(), false);
    }
}
