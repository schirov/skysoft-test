package com.shchyrov.app.skysofttest;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.shchyrov.app.skysofttest.mvp.BaseView;
import com.shchyrov.app.skysofttest.util.FragmentUtil;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

public abstract class BaseFragment extends Fragment implements BaseView {

    @Override
    public void startActivity(Class<? extends AppCompatActivity> activityClass, boolean isFinish) {
        startActivity(new Intent(getContext(), activityClass));
        if (isFinish) {
            getActivity().finish();
        }
    }

    @Override
    public void openFragment(Class<? extends Fragment> fragmentClass, Bundle bundle, int containerId, boolean toStack) {
        FragmentUtil.replaceFragment(getActivity(), fragmentClass, bundle, containerId, toStack);
    }

    @Override
    public void showText(int stringId) {
        Toast.makeText(getContext(), getString(stringId), Toast.LENGTH_LONG).show();
    }
}