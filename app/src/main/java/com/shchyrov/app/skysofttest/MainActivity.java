package com.shchyrov.app.skysofttest;

import android.os.Bundle;

import com.shchyrov.app.skysofttest.mvp.home.HomeFragment;
import com.shchyrov.app.skysofttest.util.FragmentUtil;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentUtil.openFragment(this, R.id.container_main, new HomeFragment(), false);
    }
}
