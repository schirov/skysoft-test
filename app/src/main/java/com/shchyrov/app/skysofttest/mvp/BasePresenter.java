package com.shchyrov.app.skysofttest.mvp;

public interface BasePresenter {

    void onViewCreated();
}