package com.shchyrov.app.skysofttest.mvp;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

public interface BaseView {
    void startActivity(Class<? extends AppCompatActivity> activityClass, boolean isFinish);

    void openFragment(Class<? extends Fragment> fragmentClass, Bundle bundle, int containerId, boolean toStack);

    void showText(int stringId);
}
