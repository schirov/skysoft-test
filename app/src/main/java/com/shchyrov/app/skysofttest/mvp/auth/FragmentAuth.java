package com.shchyrov.app.skysofttest.mvp.auth;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.shchyrov.app.skysofttest.BaseFragment;
import com.shchyrov.app.skysofttest.R;
import com.shchyrov.app.skysofttest.databinding.FragmentAuthBinding;
import com.shchyrov.app.skysofttest.mvp.authimpl.AuthInteractor;
import com.shchyrov.app.skysofttest.mvp.authimpl.AuthModel;
import com.shchyrov.app.skysofttest.mvp.authimpl.AuthPresenter;
import com.shchyrov.app.skysofttest.mvp.authimpl.AuthView;
import com.shchyrov.app.skysofttest.util.PreferenceUtil;

import java.util.Arrays;
import java.util.Collections;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import static android.app.Activity.RESULT_OK;

public class FragmentAuth extends BaseFragment implements AuthView {

    private static final int SIGNED_IN = 0;
    private static final int STATE_SIGNING_IN = 1;
    private static final int STATE_IN_PROGRESS = 2;
    private static final int RC_SIGN_IN = 0;

    private FragmentAuthBinding mBinding;
    private AuthPresenter mPresenter;

    private GoogleApiClient mGoogleApiClient;
    private CallbackManager mFacebookCallbackManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_auth, container, false);

        mGoogleApiClient = buildGoogleApiClient();
        mFacebookCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mFacebookCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d("OSchirov", "a1");
                    }

                    @Override
                    public void onCancel() {
                        Log.d("OSchirov", "a2");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.d("OSchirov", "a3");
                    }
                });

        mPresenter = new AuthPresenter(this, new AuthInteractor(new AuthModel()));
        mPresenter.onViewCreated();

        return mBinding.getRoot();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN && resultCode == RESULT_OK) {
            mPresenter.onGoogleSignInSuccessful(data);
        }
        if (!mGoogleApiClient.isConnecting()) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void openGoogleSignInIntent() {
        Intent signInIntent =
                Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void saveUserToPreferences(User user) {
        PreferenceUtil.setUser(getContext(), user);
    }

    @Override
    public void initBindings() {
        mBinding.setPresenter(mPresenter);
    }

    @Override
    public void setReadPermissionsFacebook() {
        mBinding.signInFacebook.setReadPermissions(Arrays.asList("email"));
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
    }

    @Override
    public void registerFacebookCallback() {
        mBinding.signInFacebook.registerCallback(mFacebookCallbackManager, mPresenter);
    }

    @Override
    public String getEnteredEmail() {
        return mBinding.email.getText().toString();
    }

    @Override
    public boolean invalidateInputFields() {
        String emailText = mBinding.email.getText().toString();
        String passText = mBinding.password.getText().toString();
        boolean isValid = true;
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailText).matches()) {
            mBinding.email.setError(getString(R.string.invalid_email));
            isValid = false;
        }
        if (passText.length() < 8 || passText.length() > 20) {
            mBinding.password.setError(getString(R.string.invalid_password));
            isValid = false;
        }
        return isValid;
    }

    private GoogleApiClient buildGoogleApiClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.google_web_client_id))
                .requestEmail()
                .build();

        return new GoogleApiClient.Builder(getContext())
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }
}