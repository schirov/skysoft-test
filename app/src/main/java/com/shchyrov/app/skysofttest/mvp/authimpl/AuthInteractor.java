package com.shchyrov.app.skysofttest.mvp.authimpl;

import com.facebook.AccessToken;
import com.shchyrov.app.skysofttest.mvp.BaseInteractor;
import com.shchyrov.app.skysofttest.mvp.auth.User;

public class AuthInteractor extends BaseInteractor {

    private AuthModel mModel;

    public AuthInteractor(AuthModel model) {
        mModel = model;
    }

    void getFacebookAccount(OnAuthCallback callback) {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null && !accessToken.isExpired()) {
            mModel.getFacebookAccount(accessToken, callback);
        }
    }

    public interface OnAuthCallback {
        void onAuthSuccess(User user);

        void onAuthError(String error);
    }

}