package com.shchyrov.app.skysofttest.mvp.authimpl;

import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.shchyrov.app.skysofttest.mvp.auth.User;

import org.json.JSONException;

public class AuthModel {
    public void getFacebookAccount(AccessToken accessToken, AuthInteractor.OnAuthCallback callback) {
        //OnCompleted is invoked once the GraphRequest is successful
        GraphRequest request = GraphRequest.newMeRequest(accessToken, (object, response) -> {
            try {
                String name = object.getString("name");
                String image = object.getJSONObject("picture").getJSONObject("data").getString("url");
                callback.onAuthSuccess(new User(name, image));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,picture.width(200)");
        request.setParameters(parameters);
        // Initiate the GraphRequest
        request.executeAsync();
    }
}
