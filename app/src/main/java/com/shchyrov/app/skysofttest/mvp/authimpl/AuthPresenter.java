package com.shchyrov.app.skysofttest.mvp.authimpl;

import android.content.Intent;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.shchyrov.app.skysofttest.MainActivity;
import com.shchyrov.app.skysofttest.mvp.BasePresenter;
import com.shchyrov.app.skysofttest.mvp.auth.User;

public class AuthPresenter implements BasePresenter, FacebookCallback<LoginResult> {

    private AuthView mView;
    private AuthInteractor mInteractor;

    public AuthPresenter(AuthView mView, AuthInteractor mInteractor) {
        this.mView = mView;
        this.mInteractor = mInteractor;
    }

    @Override
    public void onViewCreated() {
        mView.initBindings();
    }

    public void onGoogleSignInButtonClicked() {
        mView.openGoogleSignInIntent();
    }

    public void onFacebookSignInButtonClicked() {
        mView.setReadPermissionsFacebook();
        mView.registerFacebookCallback();
    }

    public void onEmailSignInButtonClicked() {
        if(mView.invalidateInputFields()) {
            mView.saveUserToPreferences(new User(mView.getEnteredEmail(), ""));
            mView.startActivity(MainActivity.class, true);
        }
    }

    public void onGoogleSignInSuccessful(Intent data) {
        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);

        try {
            GoogleSignInAccount account = task.getResult(ApiException.class);
            mView.saveUserToPreferences(new User(account.getDisplayName(), account.getPhotoUrl().toString()));
            mView.startActivity(MainActivity.class, true);
        } catch (ApiException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        mInteractor.getFacebookAccount(new AuthInteractor.OnAuthCallback() {
            @Override
            public void onAuthSuccess(User user) {
                mView.saveUserToPreferences(user);
                mView.startActivity(MainActivity.class, true);
            }

            @Override
            public void onAuthError(String error) {

            }
        });
    }

    @Override
    public void onCancel() {
    }

    @Override
    public void onError(FacebookException error) {
    }
}