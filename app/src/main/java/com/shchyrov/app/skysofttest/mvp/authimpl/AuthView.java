package com.shchyrov.app.skysofttest.mvp.authimpl;

import com.shchyrov.app.skysofttest.mvp.BaseView;
import com.shchyrov.app.skysofttest.mvp.auth.User;

public interface AuthView extends BaseView {

    void openGoogleSignInIntent();

    void saveUserToPreferences(User user);

    void initBindings();

    void setReadPermissionsFacebook();

    void registerFacebookCallback();

    String getEnteredEmail();

    boolean invalidateInputFields();
}