package com.shchyrov.app.skysofttest.mvp.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.shchyrov.app.skysofttest.BaseFragment;
import com.shchyrov.app.skysofttest.R;
import com.shchyrov.app.skysofttest.databinding.FragmentHomeBinding;
import com.shchyrov.app.skysofttest.mvp.homeimpl.HomePresenter;
import com.shchyrov.app.skysofttest.mvp.homeimpl.HomeView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;

public class HomeFragment extends BaseFragment implements HomeView {

    private FragmentHomeBinding mBinding;
    private HomePresenter mPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);

        mPresenter = new HomePresenter(this);
        mPresenter.onViewCreated();

        return mBinding.getRoot();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mPresenter.onMenuItemSelected(item);

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void initToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(mBinding.toolbar);
        mBinding.toolbar.setNavigationOnClickListener(view -> mPresenter.onNavigationDrawerWasClicked());
    }

    @Override
    public void openNavigationDrawer() {
        ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);
    }
}
