package com.shchyrov.app.skysofttest.mvp.homeimpl;

import android.os.Bundle;
import android.view.MenuItem;

import com.shchyrov.app.skysofttest.R;
import com.shchyrov.app.skysofttest.mvp.BasePresenter;
import com.shchyrov.app.skysofttest.mvp.pictureslist.FragmentPicturesList;

import static com.shchyrov.app.skysofttest.mvp.pictureslist.FragmentPicturesList.LIST_TYPE;

public class HomePresenter implements BasePresenter {

    private HomeView mView;

    public HomePresenter(HomeView mView) {
        this.mView = mView;
    }

    @Override
    public void onViewCreated() {
        mView.initToolbar();
    }

    public void onNavigationDrawerWasClicked() {
        mView.openNavigationDrawer();
    }

    public void onMenuItemSelected(MenuItem item) {
        Bundle data = new Bundle();
        switch (item.getItemId()) {
            case R.id.linear_data: {
                data.putInt(LIST_TYPE, FragmentPicturesList.ListType.LINEAR.ordinal());
                break;
            }
            case R.id.grid_data: {
                data.putInt(LIST_TYPE, FragmentPicturesList.ListType.GRID.ordinal());
                break;
            }
        }
        mView.openFragment(FragmentPicturesList.class, data, R.id.container_main, true);
    }
}
