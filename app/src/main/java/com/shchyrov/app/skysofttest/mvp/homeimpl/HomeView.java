package com.shchyrov.app.skysofttest.mvp.homeimpl;

import com.shchyrov.app.skysofttest.mvp.BaseView;

public interface HomeView extends BaseView {

    void initToolbar();

    void openNavigationDrawer();
}