package com.shchyrov.app.skysofttest.mvp.navdrawer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shchyrov.app.skysofttest.BaseFragment;
import com.shchyrov.app.skysofttest.R;
import com.shchyrov.app.skysofttest.databinding.FragmentNavigationDrawerBinding;
import com.shchyrov.app.skysofttest.mvp.navdrawerimpl.NavigationDrawerInteractor;
import com.shchyrov.app.skysofttest.mvp.navdrawerimpl.NavigationDrawerPresenter;
import com.shchyrov.app.skysofttest.mvp.navdrawerimpl.NavigationDrawerView;
import com.shchyrov.app.skysofttest.util.PreferenceUtil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;

public class FragmentNavigationDrawer extends BaseFragment implements NavigationDrawerView {

    private FragmentNavigationDrawerBinding mBinding;
    private NavigationDrawerPresenter mPresenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_navigation_drawer, container, false);

        mPresenter = new NavigationDrawerPresenter(this, new NavigationDrawerInteractor());
        mPresenter.onViewCreated();

        return mBinding.getRoot();
    }

    @Override
    public void initBindings() {
        mBinding.setUser(PreferenceUtil.getUser(getContext()));
    }

    @Override
    public void initMenu() {
        mBinding.recyclerNavigation.setHasFixedSize(true);
        mBinding.recyclerNavigation.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.recyclerNavigation.setAdapter(new NavigationDrawerAdapter(mPresenter));
    }

    @Override
    public void closeDrawer() {
        ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout)).closeDrawer(GravityCompat.START);
    }

    @Override
    public void removeUserFromPreference() {
        PreferenceUtil.setUser(getContext(), null);
    }
}