package com.shchyrov.app.skysofttest.mvp.navdrawer;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.shchyrov.app.skysofttest.R;
import com.shchyrov.app.skysofttest.databinding.ItemNavigationDrawerBinding;
import com.shchyrov.app.skysofttest.mvp.navdrawerimpl.NavigationDrawerPresenter;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.ViewHolder> {

    private NavigationDrawerPresenter mPresenter;

    public NavigationDrawerAdapter(NavigationDrawerPresenter mPresenter) {
        this.mPresenter = mPresenter;
    }

    @NonNull
    @Override
    public NavigationDrawerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_navigation_drawer, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull NavigationDrawerAdapter.ViewHolder holder, int position) {
        holder.bind(NavigationDrawerEnum.values()[position]);
        holder.mBinding.getRoot().setOnClickListener(v -> {
            mPresenter.onMenuItemSelected(position);
        });
    }

    @Override
    public int getItemCount() {
        return NavigationDrawerEnum.values().length;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ItemNavigationDrawerBinding mBinding;

        ViewHolder(@NonNull ItemNavigationDrawerBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        void bind(NavigationDrawerEnum data) {
            mBinding.setData(data);
        }
    }

}
