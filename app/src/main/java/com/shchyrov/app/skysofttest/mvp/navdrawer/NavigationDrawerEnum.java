package com.shchyrov.app.skysofttest.mvp.navdrawer;

import com.shchyrov.app.skysofttest.mvp.profile.FragmentProfile;

import androidx.fragment.app.Fragment;

public enum NavigationDrawerEnum {

    PROFILE("My Profile", FragmentProfile.class),
    LOGOUT("Log out", null);

    private String name;
    private Class<? extends Fragment> className;

    NavigationDrawerEnum(String name, Class<? extends Fragment> className) {
        this.name = name;
        this.className = className;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class<? extends Fragment> getClassName() {
        return className;
    }

    public void setClassName(Class<? extends Fragment> className) {
        this.className = className;
    }
}