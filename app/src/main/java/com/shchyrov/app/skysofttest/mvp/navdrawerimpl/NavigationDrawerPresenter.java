package com.shchyrov.app.skysofttest.mvp.navdrawerimpl;

import android.os.Bundle;

import com.shchyrov.app.skysofttest.AuthActivity;
import com.shchyrov.app.skysofttest.R;
import com.shchyrov.app.skysofttest.mvp.BasePresenter;
import com.shchyrov.app.skysofttest.mvp.navdrawer.NavigationDrawerEnum;

import androidx.fragment.app.Fragment;

public class NavigationDrawerPresenter implements BasePresenter {

    private NavigationDrawerView mView;
    private NavigationDrawerInteractor mInteractor;

    public NavigationDrawerPresenter(NavigationDrawerView mView, NavigationDrawerInteractor mInteractor) {
        this.mView = mView;
        this.mInteractor = mInteractor;
    }

    @Override
    public void onViewCreated() {
        mView.initBindings();
        mView.initMenu();
    }

    public void onMenuItemSelected(int position) {
        Class<? extends Fragment> classFragment = NavigationDrawerEnum.values()[position].getClassName();
        if (classFragment != null) {
            mView.openFragment(classFragment, new Bundle(), R.id.container_main, true);
            mView.closeDrawer();
            return;
        }
        if (NavigationDrawerEnum.values()[position] == NavigationDrawerEnum.LOGOUT) {
            mView.removeUserFromPreference();
            mView.startActivity(AuthActivity.class, true);
        }
    }
}
