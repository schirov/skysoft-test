package com.shchyrov.app.skysofttest.mvp.navdrawerimpl;

import com.shchyrov.app.skysofttest.mvp.BaseView;

public interface NavigationDrawerView extends BaseView {

    void initBindings();

    void initMenu();

    void closeDrawer();

    void removeUserFromPreference();
}