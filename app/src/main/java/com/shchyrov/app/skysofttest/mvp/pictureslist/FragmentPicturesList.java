package com.shchyrov.app.skysofttest.mvp.pictureslist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shchyrov.app.skysofttest.BaseFragment;
import com.shchyrov.app.skysofttest.R;
import com.shchyrov.app.skysofttest.databinding.FragmentPictureListBinding;
import com.shchyrov.app.skysofttest.mvp.pictureslistimpl.PictureListInteractor;
import com.shchyrov.app.skysofttest.mvp.pictureslistimpl.PictureListModel;
import com.shchyrov.app.skysofttest.mvp.pictureslistimpl.PictureListPresenter;
import com.shchyrov.app.skysofttest.mvp.pictureslistimpl.PictureListView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

public class FragmentPicturesList extends BaseFragment implements PictureListView {

    public static final String LIST_TYPE = "list_type";
    private static final Integer SPAN_COUNT = 4;
    private FragmentPictureListBinding mBinding;
    private PictureListPresenter mPresenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_picture_list, container, false);

        mPresenter = new PictureListPresenter(this, new PictureListInteractor(new PictureListModel()));
        mPresenter.onViewCreated();

        return mBinding.getRoot();
    }

    @Override
    public Bundle getBundles() {
        return getArguments();
    }

    @Override
    public void setAdapter(ListType type, List<String> images) {
        mBinding.recyclerPictures.setHasFixedSize(true);
        mBinding.recyclerPictures.setLayoutManager(type == ListType.LINEAR
                ? new LinearLayoutManager(getContext())
                : new GridLayoutManager(getContext(), SPAN_COUNT));
        mBinding.recyclerPictures.setAdapter(new PicturesListAdapter(images, type));
    }

    public enum ListType {
        LINEAR,
        GRID
    }
}
