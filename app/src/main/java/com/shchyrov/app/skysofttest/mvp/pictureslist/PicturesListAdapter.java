package com.shchyrov.app.skysofttest.mvp.pictureslist;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.shchyrov.app.skysofttest.R;
import com.shchyrov.app.skysofttest.databinding.ItemPictureListGridBinding;
import com.shchyrov.app.skysofttest.databinding.ItemPictureListLinearBinding;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class PicturesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> mList;
    private FragmentPicturesList.ListType mType;

    public PicturesListAdapter(List<String> mList, FragmentPicturesList.ListType mType) {
        this.mList = mList;
        this.mType = mType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (mType == FragmentPicturesList.ListType.GRID) {
            return new GridViewHolder(DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()),
                    R.layout.item_picture_list_grid, parent, false));
        } else {
            return new LinearViewHolder(DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()),
                    R.layout.item_picture_list_linear, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (mType == FragmentPicturesList.ListType.GRID) {
            ((GridViewHolder) holder).bind(mList.get(position));
        } else {
            ((LinearViewHolder) holder).bind(mList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class LinearViewHolder extends RecyclerView.ViewHolder {

        private ItemPictureListLinearBinding mBinding;

        public LinearViewHolder(ItemPictureListLinearBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        void bind(String image) {
            mBinding.setImage(image);
            mBinding.setPosition(String.valueOf(mList.indexOf(image)));
        }
    }

    public class GridViewHolder extends RecyclerView.ViewHolder {

        private ItemPictureListGridBinding mBinding;

        public GridViewHolder(@NonNull ItemPictureListGridBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        void bind(String image) {
            mBinding.setImage(image);
        }
    }
}
