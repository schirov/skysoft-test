package com.shchyrov.app.skysofttest.mvp.pictureslistimpl;

import com.shchyrov.app.skysofttest.mvp.BaseInteractor;

import java.util.List;

public class PictureListInteractor extends BaseInteractor {

    private PictureListModel mModel;

    public PictureListInteractor(PictureListModel model) {
        this.mModel = model;
    }

    public void getListData(OnResponseReceiveListener listener) {
        mModel.getListData(listener);
    }

    interface OnResponseReceiveListener {
        void onReceive(List<String> images);

        void onError(String errorMessage);
    }

}