package com.shchyrov.app.skysofttest.mvp.pictureslistimpl;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PictureListModel {
    private static final String URL = "https://jsonplaceholder.typicode.com/photos";

    public void getListData(PictureListInteractor.OnResponseReceiveListener listener) {
        AndroidNetworking.get(URL)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        List<String> result = new ArrayList<>();
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                result.add(response.getJSONObject(i).getString("thumbnailUrl").concat(".png"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (i >= 9) {
                                break;
                            }
                        }
                        listener.onReceive(result);
                    }

                    @Override
                    public void onError(ANError anError) {
                        listener.onError(anError.getMessage());
                    }
                });
    }
}
