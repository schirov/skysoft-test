package com.shchyrov.app.skysofttest.mvp.pictureslistimpl;

import android.os.Bundle;

import com.shchyrov.app.skysofttest.R;
import com.shchyrov.app.skysofttest.mvp.BasePresenter;
import com.shchyrov.app.skysofttest.mvp.pictureslist.FragmentPicturesList;

import java.util.List;

public class PictureListPresenter implements BasePresenter {

    private PictureListView mView;
    private PictureListInteractor mInteractor;

    public PictureListPresenter(PictureListView view, PictureListInteractor interactor) {
        this.mView = view;
        this.mInteractor = interactor;
    }

    @Override
    public void onViewCreated() {
        mInteractor.getListData(new PictureListInteractor.OnResponseReceiveListener() {
            @Override
            public void onReceive(List<String> images) {
                Bundle bundle = mView.getBundles();
                mView.setAdapter(FragmentPicturesList.ListType.values()
                        [bundle.getInt(FragmentPicturesList.LIST_TYPE)], images);
            }

            @Override
            public void onError(String errorMessage) {
                mView.showText(R.string.error_load_list);
            }
        });
    }
}
