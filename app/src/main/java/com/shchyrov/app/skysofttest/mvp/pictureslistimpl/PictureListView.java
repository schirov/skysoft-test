package com.shchyrov.app.skysofttest.mvp.pictureslistimpl;

import android.os.Bundle;

import com.shchyrov.app.skysofttest.mvp.BaseView;
import com.shchyrov.app.skysofttest.mvp.pictureslist.FragmentPicturesList;

import java.util.List;

public interface PictureListView extends BaseView {
    Bundle getBundles();

    void setAdapter(FragmentPicturesList.ListType type, List<String> images);
}
