package com.shchyrov.app.skysofttest.mvp.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shchyrov.app.skysofttest.BaseFragment;
import com.shchyrov.app.skysofttest.R;
import com.shchyrov.app.skysofttest.databinding.FragmentProfileBinding;
import com.shchyrov.app.skysofttest.mvp.auth.User;
import com.shchyrov.app.skysofttest.mvp.navdrawer.FragmentNavigationDrawer;
import com.shchyrov.app.skysofttest.mvp.profileimpl.ProfilePresenter;
import com.shchyrov.app.skysofttest.mvp.profileimpl.ProfileView;
import com.shchyrov.app.skysofttest.util.PreferenceUtil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

public class FragmentProfile extends BaseFragment implements ProfileView {

    private FragmentProfileBinding mBinding;
    private ProfilePresenter mPresenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);

        mPresenter = new ProfilePresenter(this);
        mPresenter.onViewCreated();

        return mBinding.getRoot();
    }

    @Override
    public void initBindings(User user) {
        mBinding.setUser(user);
        mBinding.setPresenter(mPresenter);
    }

    @Override
    public void saveUserToPreferences(User user) {
        PreferenceUtil.setUser(getContext(), user);
    }

    @Override
    public User getUserFromPreferences() {
        return PreferenceUtil.getUser(getContext());
    }

    @Override
    public String getUserNameFromInput() {
        return mBinding.userName.getText().toString();
    }

    @Override
    public void updateDataInNavigationDrawer() {
        ((FragmentNavigationDrawer) getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer)).initBindings();
    }
}