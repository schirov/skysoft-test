package com.shchyrov.app.skysofttest.mvp.profileimpl;

import com.shchyrov.app.skysofttest.R;
import com.shchyrov.app.skysofttest.mvp.BasePresenter;
import com.shchyrov.app.skysofttest.mvp.auth.User;

public class ProfilePresenter implements BasePresenter {

    private ProfileView mView;

    public ProfilePresenter(ProfileView mView) {
        this.mView = mView;
    }

    @Override
    public void onViewCreated() {
        mView.initBindings(mView.getUserFromPreferences());
    }

    public void onSaveButtonWasClicked() {
        User user = mView.getUserFromPreferences();
        user.setName(mView.getUserNameFromInput());
        mView.saveUserToPreferences(user);
        mView.updateDataInNavigationDrawer();
        mView.showText(R.string.user_updated);
    }
}
