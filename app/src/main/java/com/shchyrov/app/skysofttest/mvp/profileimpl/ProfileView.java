package com.shchyrov.app.skysofttest.mvp.profileimpl;

import com.shchyrov.app.skysofttest.mvp.BaseView;
import com.shchyrov.app.skysofttest.mvp.auth.User;

public interface ProfileView extends BaseView {

    void initBindings(User user);

    void saveUserToPreferences(User user);

    User getUserFromPreferences();

    String getUserNameFromInput();

    void updateDataInNavigationDrawer();
}