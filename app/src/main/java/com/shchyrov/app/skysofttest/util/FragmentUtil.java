package com.shchyrov.app.skysofttest.util;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

public final class FragmentUtil {

    private FragmentUtil() {
    }

    public static void openFragment(AppCompatActivity activity, int containerId, Fragment fragment, boolean toStack) {
        FragmentTransaction transaction = activity.getSupportFragmentManager()
                .beginTransaction();
        transaction.add(containerId, fragment);
        if (toStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    public static void replaceFragment(FragmentActivity activity, Class<? extends Fragment> fragmentClass, Bundle bundle, int containerId, boolean toStack) {
        Fragment fragment = null;
        try {
            fragment = (Fragment) Class.forName(fragmentClass.getName()).newInstance();
        } catch (Exception e) {
        }
        fragment.setArguments(bundle);
        FragmentTransaction transaction = activity.getSupportFragmentManager()
                .beginTransaction();
        transaction.replace(containerId, fragment);
        if (toStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }
}