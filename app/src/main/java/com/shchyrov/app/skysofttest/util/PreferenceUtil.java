package com.shchyrov.app.skysofttest.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.shchyrov.app.skysofttest.mvp.auth.User;

import java.lang.reflect.Type;

import androidx.annotation.NonNull;

public class PreferenceUtil {

    private static final String PREFERENCES = "preferences";
    private static final String PREF_USER = "user";

    private static SharedPreferences sp;
    private static SharedPreferences.Editor editor;

    public static void setUser(Context context, User user) {
        sp = getMainSharedPreferences(context);
        editor = sp.edit();
        editor.putString(PREF_USER, new Gson().toJson(user));
        editor.apply();
    }

    public static User getUser(@NonNull Context context) {
        sp = getMainSharedPreferences(context);
        String value = sp.getString(PREF_USER, "");
        if (TextUtils.isEmpty(value)) {
            return null;
        }
        Class c = null;
        try {
            c = Class.forName(User.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return new Gson().fromJson(value, (Type) c);
    }

    private static SharedPreferences getMainSharedPreferences(Context context) {
        return context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
    }
}